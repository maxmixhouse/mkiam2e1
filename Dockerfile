FROM node:boron

#Crear el directorio de la app
RUN  mkdir -p /usr/src/app
WORKDIR /usr/src/app

#Instalar las dependencias
COPY package.json /usr/src/app
RUN npm install

#Etiquetar el código
COPY . /usr/src/app

EXPOSE 8083
CMD ["npm","start"]
