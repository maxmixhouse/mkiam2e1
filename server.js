
'use strict';

const express = require('express'); //Esto hace que utilice express
const path = require('path'); //Esto hace que utilice path

//Constantes
const PORT = 8088;

//App
const app = express();
//la siguiente function la invova express, peticion --> request ,
//respuesta -->response

app.use(express.static(__dirname));

app.get('/',function(peticion, respuesta){
  //respuesta.send("Hola mundo leas\n");
  respuesta.sendFile(path.join(__dirname+'/inde.html'));
  //respuesta.send(peticion);
});

app.listen(PORT);
console.log("Express funcionando en el puerto " + PORT);
